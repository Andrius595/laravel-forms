@php(FormsAssets::add(['summernote']))

<x-group :name="$name"
         :label="$label"
         :required="$required"
         :for="$for"
         :wrapper-class="$wrapperClass"
         :label-class="$labelClass"
         :row-class="$rowClass"
         :tooltip="$tooltip">

    <textarea name="{{ $name }}"
              {{ $attributes->merge(['class' => 'uk-textarea rich-textarea','uk-form-danger' => $errors->has($name)]) }}
              @if($id) id="{{ $id }}" @endif
              @if($placeholder) placeholder="{{ $placeholder }}" @endif
              @if($required) required @endif>{{ $slot}}</textarea>

</x-group>
