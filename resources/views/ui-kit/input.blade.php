<x-group :name="$name"
               :label="$label"
               :required="$required"
               :for="$for"
               :wrapper-class="$wrapperClass"
               :label-class="$labelClass"
               :row-class="$rowClass"
               :tooltip="$tooltip">

    <input name="{{ $name }}"
           {{ $attributes->merge(['class' => 'uk-input','uk-form-danger' => $errors->has($name)]) }}
           @if($id) id="{{ $id }}" @endif
           @if($value) value="{{ $value }}" @endif
           @if($required) required @endif>

</x-group>
