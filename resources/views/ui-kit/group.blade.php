<div class="uk-margin {{ $wrapperClass }}" uk-margin>

    @if($label)
        <x-label for="{{ $for }}"
                       class="{{ $labelClass }}"
                       tooltip="{{ $tooltip }}">
            {{ $label }}@if($required)*@endif
        </x-label>
    @endif

    <div class="uk-form-controls @if($rowClass){{ $rowClass }}@endif @error($name) has-errors @enderror">
        {{ $slot }}
    </div>

    @error($name)
        <x-error>{{ $message }}</x-error>
    @enderror
</div>
