<div class="uk-margin">
    <label><input {{ $attributes->merge(['class' => 'uk-checkbox']) }} type="checkbox" @isset($value) value="{{ $value }}" @endisset
                  name="{{ $name }}"> {{ $slot }}
    </label>
</div>
