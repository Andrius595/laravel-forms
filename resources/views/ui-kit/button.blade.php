<button type="{{ $type }}" {{ $attributes->merge(['class' => 'uk-button uk-border-pill']) }}>
    {{ $slot }}
</button>
