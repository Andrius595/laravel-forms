<script>
    $(document).ready(function () {

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).on('click', '.delete-image', function (e) {
            e.preventDefault();

            if (confirm('Delete selected item?')) {
                var $this = $(this),
                    key = $this.attr('data-image');

                $.ajax({
                    url: '{{ $ajaxUrl }}',
                    method: "POST",
                    data: {
                        '_method': 'DELETE',
                        'key': key,
                    },
                    type: 'json'
                }).done(function (resp) {
                    if (resp) {
                        $('.' + key + '_row').remove();
                    }
                });
            }
        });
    });
</script>
