<x-group :name="$name"
               :label="$label"
               :required="$required"
               :for="$for"
               :wrapper-class="$wrapperClass"
               :label-class="$labelClass"
               :row-class="$rowClass"
               :tooltip="$tooltip">

    <textarea name="{{ $name }}"
              {{ $attributes->merge(['class' => 'uk-textarea','uk-form-danger' => $errors->has($name)]) }}
              @if($id) id="{{ $id }}" @endif
              @if($required) required @endif>{{ $slot}}</textarea>

</x-group>
