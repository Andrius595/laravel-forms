<x-group :name="$name"
         :label="$label"
         :required="$required"
         :wrapper-class="$wrapperClass"
         :label-class="$labelClass"
         :row-class="$rowClass"
         :tooltip="$tooltip">

    @foreach($options as $value => $title)
        <label>
            <input {{ $attributes->merge(['class' => 'uk-radio', 'uk-form-danger' => $errors->has($name)]) }}
                   type="radio" name="{{ $name }}"
                   value="{{ $value }}" @if((string)$value === (string)$checkedValue) checked @endif> {{ $title }}
        </label>
    @endforeach

</x-group>
