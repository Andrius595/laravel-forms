<x-group :name="$name"
               :label="$label"
               :required="$required"
               :for="$for"
               :wrapper-class="$wrapperClass"
               :label-class="$labelClass"
               :row-class="$rowClass"
               :tooltip="$tooltip">

    <select name="{{ $name }}"
            {{$attributes->merge(['class' => 'uk-select', 'uk-form-danger' => $errors->has($name)]) }}
            {{ $multiple ? 'multiple' : '' }}
            @if($id) id="{{ $id }}" @endif
            @if($required) required @endif>

        @if($emptyValueLabel)
            <option {{ $multiple ? 'disabled' : '' }} value="{{ $emptyValue }}">
                {{ $emptyValueLabel }}
            </option>
        @endif

        @foreach($options as $value => $name)
            <option value="{{ $value }}"
                    @if ((string)$selected === (string)$value) selected @endif >
                {{ $name }}
            </option>
        @endforeach
    </select>

</x-group>
