<x-radio name="active"
         label="{{ __('main.active') }}"
         checked-value="{{ $checkedValue }}"
         :options="['1' => __('main.yes'), '0' => __('main.no')]"
         required/>
