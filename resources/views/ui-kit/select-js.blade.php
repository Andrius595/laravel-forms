@php(FormsAssets::add(['scripts','select2']))

<x-group :name="$name"
         :label="$label"
         :required="$required"
         :for="$for"
         :wrapper-class="$wrapperClass"
         :label-class="$labelClass"
         :row-class="$rowClass"
         :tooltip="$tooltip">

    <select name="{{ $name }}" @if($emptyValueLabel) data-placeholder="{{ $emptyValueLabel }}" @endif
            data-allow-clear="{{ $allowClear }}"
            data-ajax--cache="{{ $ajaxCache }}"
            data-ajax--data-type="{{ $ajaxDataType }}"
            data-ajax--delay="250"
            data-selected="{{ (string)$selected }}"
            {{ $attributes->merge(['class' => 'uk-select select2-el js-states form-control', 'uk-form-danger' => $errors->has($name)]) }}
            {{ $multiple ? 'multiple' : '' }}
            @if($id) id="{{ $id }}" @endif
            @if($required) required @endif>

        @if($options)
            @foreach($options as $value => $name)
                <option value="{{ $value }}"
                        @if ((string)$selected === (string)$value) selected @endif >
                    {{ $name }}
                </option>
            @endforeach
        @endif
    </select>

</x-group>
