<form {!! $attributes->merge(['class' => 'uk-form-horizontal']) !!}
      method="{{ $spoofMethod ? 'POST' : $method }}">

    @unless(in_array($method, ['HEAD', 'GET', 'OPTIONS']))
        @csrf
    @endunless

    @if($spoofMethod)
        @method($method)
    @endif

    {!! $slot !!}
</form>
