@php(FormsAssets::add('scripts'))
<div class="{{ $wrapperClass }}">
    @isset($label)
    <x-label :for="$name">{{ $label }}</x-label>
    @endisset

    <div class="uk-form-controls">
        <div uk-form-custom="target: true">
            <input type="file" id="{{ $name }}" name="{{ $name }}">
            <input class="uk-input"
                   type="text" placeholder="{{ __('main.select_file') }}" disabled>
        </div>
    </div>
    @error($name)
    <x-error>{{ $error }}</x-error>
    @enderror
</div>
@if($file)
    <div class="uk-margin {{ $name }}_row">
        <label class="uk-form-label"></label>
        <div class="uk-form-controls">
            <div class="uk-card uk-card-default uk-card-small uk-width-medium">
                <div class="uk-card-media-top">
                    <img src="{{ asset('storage/' . $file) }}" alt="">
                </div>
                <div class="uk-card-body uk-text-center">
                    <a class="uk-button uk-button-danger uk-button-small delete-image" href="#"
                       data-image="{{ $name }}">
                        Delete image
                    </a>
                </div>
            </div>
        </div>
    </div>
@endif
