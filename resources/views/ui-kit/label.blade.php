<label {{ $attributes->merge(['class' => 'uk-form-label']) }}>
    {{ $slot }} @if($tooltip) <span uk-icon="icon: info" uk-tooltip="title: {{ $tooltip }}"></span> @endif
</label>
