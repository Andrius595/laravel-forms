let app = {
    init: function () {
        let $select2 = $('.select2-el'),
            $richTextarea = $('.rich-textarea'),
            $delete_file = $('.delete-file');

        if ($select2.length) {
            app.select2($select2);
        }

        if ($richTextarea.length) {
            app.summernote($richTextarea);
        }

        if($delete_file.length) {
            app.delete_file($delete_file)
        }
    },
    ajax: function (params) {
        let defaults = {
            type: 'POST',
            cache: false,
            dataType: 'json',
        }

        let options = $.extend({}, defaults, params);
        app.callAjax(options);
    },
    callAjax: function (options) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: options.type,
            url: options.url,
            cache: options.cache,
            data: options.data
        }).done(function (resp) {
            if (typeof options.done === 'function') {
                options.done(resp);
            }
        }).fail(function (jqXHR, textStatus, errorThrown) {
            // TODO remove
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
        });
    },
    select2: function ($el) {
        $el.each(function (key, el) {
            let $sel = $(el),
                selected = $sel.attr('data-selected'),
                route = $sel.attr('data-ajax--url');

            $sel.select2({
                theme: 'classic'
            });

            if (selected) {
                setTimeout(function () {
                    $.ajax({
                        type: 'GET',
                        url: route + '/' + selected
                    }).then(function (data) {
                        if (null !== data) {
                            // create the option and append to Select2
                            let option = new Option(data.name, data.id, true, true);
                            $sel.append(option).trigger('change');

                            // manually trigger the `select2:select` event
                            $sel.trigger({
                                type: 'select2:select',
                                params: {
                                    data: data
                                }
                            });
                        }
                    });
                }, 200);
            }
        });
    },
    summernote: function ($el) {
        $el.each(function (key, el) {
            let $sel = $(el);

            $sel.summernote({
                height: 300,
            });
        });
    },
    delete_file: function ($el) {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $(document).on('click', '.delete-image', function (e) {
            e.preventDefault();

            if (confirm('Delete selected item?')) {
                var $this = $(this),
                    key = $this.attr('data-image');

                $.ajax({
                    url: '{{ $ajaxUrl }}',
                    method: "POST",
                    data: {
                        '_method': 'DELETE',
                        'key': key,
                    },
                    type: 'json'
                }).done(function (resp) {
                    if (resp) {
                        $('.' + key + '_row').remove();
                    }
                });
            }
        });
    }
}

$(function () {
    app.init();
});
