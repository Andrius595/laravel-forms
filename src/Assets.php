<?php

namespace Andrius595\LaravelForms;

use Closure;

class Assets
{
    /**
     * Regex to match against a filename/url to determine if it is a CSS asset.
     *
     * @var string
     */
    protected string $css_regex = '/.\.css$/i';

    /**
     * Regex to match against a filename/url to determine if it is a JavaScript asset.
     *
     * @var string
     */
    protected string $js_regex = '/.\.js$/i';

    /**
     * Directory for local CSS assets.
     * Relative to your public directory ('public_dir').
     * No trailing slash!.
     *
     * @var string
     */
    protected string $css_dir = 'css';

    /**
     * Directory for local JavaScript assets.
     * Relative to your public directory ('public_dir').
     * No trailing slash!.
     *
     * @var string
     */
    protected string $js_dir = 'js';

    /**
     * Directory for local package assets.
     * Relative to your public directory ('public_dir').
     * No trailing slash!.
     *
     * @var string
     */
    protected string $packages_dir = 'packages';

    /**
     * Available collections.
     * Each collection is an array of assets.
     * Collections may also contain other collections.
     *
     * @var array
     */
    protected array $collections = array();

    /**
     * CSS files already added.
     * Not accepted as an option of config() method.
     *
     * @var array
     */
    protected array $css = array();

    /**
     * JavaScript files already added.
     * Not accepted as an option of config() method.
     *
     * @var array
     */
    protected array $js = array();

    /**
     * Class constructor.
     */
    public function __construct(array $options = array())
    {
        // Forward config options
        if ($options) {
            $this->config($options);
        }
    }

    /**
     * Set up configuration options.
     *
     * All the class properties except 'js' and 'css' are accepted here.
     * Also, an extra option 'autoload' may be passed containing an array of
     * assets and/or collections that will be automatically added on startup.
     */
    public function config(array $config): Assets
    {
        // Set regex options
        foreach (array('asset_regex', 'css_regex', 'js_regex') as $option) {
            if (isset($config[$option]) && (@preg_match($config[$option], null) !== false)) {
                $this->$option = $config[$option];
            }
        }

        // Set common options
        foreach (array('css_dir', 'js_dir', 'packages_dir') as $option) {
            if (isset($config[$option])) {
                $this->$option = $config[$option];
            }
        }

        // Set collections
        if (isset($config['collections']) && is_array($config['collections'])) {
            $this->collections = $config['collections'];
        }

        // Autoload assets
        if (isset($config['autoload']) && is_array($config['autoload'])) {
            foreach ($config['autoload'] as $asset) {
                $this->add($asset);
            }
        }

        return $this;
    }

    /**
     * Add an asset or a collection of assets.
     *
     * It automatically detects the asset type (JavaScript, CSS or collection).
     * You may add more than one asset passing an array as argument.
     */
    public function add(mixed $asset): Assets
    {
        // More than one asset
        if (is_array($asset)) {
            foreach ($asset as $a) {
                $this->add($a);
            }
        } // Collection
        elseif (isset($this->collections[$asset])) {
            $this->add($this->collections[$asset]);
        } // JavaScript asset
        elseif (preg_match($this->js_regex, $asset)) {
            $this->addJs($asset);
        } // CSS asset
        elseif (preg_match($this->css_regex, $asset)) {
            $this->addCss($asset);
        }

        return $this;
    }

    /**
     * Add a CSS asset.
     *
     * It checks for duplicates.
     * You may add more than one asset passing an array as argument.
     */
    public function addCss(mixed $asset): Assets
    {
        if (is_array($asset)) {
            foreach ($asset as $a) {
                $this->addCss($a);
            }

            return $this;
        }

        if (!$this->isRemoteLink($asset)) {
            $asset = $this->buildLocalLink($asset, $this->css_dir);
        }

        if (!in_array($asset, $this->css, true)) {
            $this->css[] = $asset;
        }

        return $this;
    }

    /**
     * Build the CSS `<link>` tags.
     *
     * Accepts an array of $attributes for the HTML tag.
     * You can take control of the tag rendering by
     * providing a closure that will receive an array of assets.
     */
    public function css(array|Closure $attributes = null): string
    {
        if (!$this->css) {
            return '';
        }

        $assets = $this->css;

        if ($attributes instanceof Closure) {
            return $attributes->__invoke($assets);
        }

        // Build attributes
        $attributes = (array)$attributes;
        unset($attributes['href']);

        if (!array_key_exists('type', $attributes)) {
            $attributes['type'] = 'text/css';
        }

        if (!array_key_exists('rel', $attributes)) {
            $attributes['rel'] = 'stylesheet';
        }

        $attributes = $this->buildTagAttributes($attributes);

        // Build tags
        $output = '';
        foreach ($assets as $asset) {
            $output .= '<link href="' . $asset . '"' . $attributes . " />\n";
        }

        return $output;
    }

    /**
     * Add a JavaScript asset.
     *
     * It checks for duplicates.
     * You may add more than one asset passing an array as argument.
     */
    public function addJs(mixed $asset): Assets
    {
        if (is_array($asset)) {
            foreach ($asset as $a) {
                $this->addJs($a);
            }

            return $this;
        }

        if (!$this->isRemoteLink($asset)) {
            $asset = $this->buildLocalLink($asset, $this->js_dir);
        }

        if (!in_array($asset, $this->js, true)) {
            $this->js[] = $asset;
        }

        return $this;
    }

    /**
     * Build the JavaScript `<script>` tags.
     *
     * Accepts an array of $attributes for the HTML tag.
     * You can take control of the tag rendering by
     * providing a closure that will receive an array of assets.
     */
    public function js(array|Closure $attributes = null): string
    {
        if (!$this->js) {
            return '';
        }

        $assets = $this->js;

        if ($attributes instanceof Closure) {
            return $attributes->__invoke($assets);
        }

        // Build attributes
        $attributes = (array)$attributes;
        unset($attributes['src']);

        if (!array_key_exists('type', $attributes)) {
            $attributes['type'] = 'text/javascript';
        }

        $attributes = $this->buildTagAttributes($attributes);

        // Build tags
        $output = '';
        foreach ($assets as $asset) {
            $output .= '<script src="' . $asset . '"' . $attributes . "></script>\n";
        }

        return $output;
    }

    /**
     * Build link to local asset.
     *
     * Detects packages links.
     */
    protected function buildLocalLink(string $asset, string $dir): string
    {
        $package = $this->assetIsFromPackage($asset);

        if ($package === false) {
            return $dir . '/' . $asset;
        }

        return $this->packages_dir . '/' . $package[0] . '/' . $package[1] . '/' . ltrim($dir, '/') . '/' . $package[2];
    }

    /**
     * Determine whether a link is local or remote.
     *
     * Understands both "http://" and "https://" as well as protocol agnostic links "//"
     */
    protected function isRemoteLink(string $link): bool
    {
        return (str_starts_with($link, 'http://') or str_starts_with($link, 'https://') or str_starts_with($link, '//'));
    }

    /**
     * Determine whether an asset is normal or from a package.
     */
    protected function assetIsFromPackage(string $asset): bool|array
    {
        if (preg_match('{^([A-Za-z0-9_.-]+)/([A-Za-z0-9_.-]+):(.*)$}', $asset, $matches)) {
            return array_slice($matches, 1, 3);
        }

        return false;
    }

    /**
     * Build an HTML attribute string from an array.
     */
    public function buildTagAttributes(array $attributes): string
    {
        $html = array();

        foreach ($attributes as $key => $value) {
            if (is_null($value)) {
                continue;
            }

            if (is_numeric($key)) {
                $key = $value;
            }

            $html[] = $key . '="' . htmlentities($value, ENT_QUOTES, 'UTF-8', false) . '"';
        }

        return (count($html) > 0) ? ' ' . implode(' ', $html) : '';
    }
}
