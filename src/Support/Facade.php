<?php

namespace Andrius595\LaravelForms\Support;

use \Illuminate\Support\Facades\Facade as BaseFacade;

class Facade extends BaseFacade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'andrius595.forms.assets';
    }
}
