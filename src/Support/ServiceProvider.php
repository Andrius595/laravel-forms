<?php

namespace Andrius595\LaravelForms\Support;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider as BaseServiceProvider;
use Andrius595\LaravelForms\Assets;

class ServiceProvider extends BaseServiceProvider
{
    public function boot(): void
    {
        // Creating Facade instance
        AliasLoader::getInstance()->alias('FormsAssets', Facade::class);

        // Publishing views and config
        if ($this->app->runningInConsole()) {
            $this->publishes([
                __DIR__ . '/../../config/config.php' => config_path('laravel-forms.php'),
            ], 'config');

            $this->publishes([
                __DIR__ . '/../../resources/views' => resource_path('views/vendor/laravel-forms'),
            ], 'views');

            $this->publishes([
                __DIR__ . '/../../resources/assets/js' => public_path('js/vendor/laravel-forms'),
            ], 'javascript');


        }

        // Registering views
        $this->loadViewsFrom(__DIR__ . '/../../resources/views', 'laravel-forms');

        // Registering Blade components
        Collection::make(config('laravel-forms.components'))->each(
            fn($component, $alias) => Blade::component($component['class'], $alias)
        );
    }

    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__ . '/../../config/config.php', 'laravel-forms');

        $config = $this->app['config']->get('laravel-forms', []);
        $this->app->singleton("andrius595.forms.assets", function ($app) use ($config) {
            if (!isset($config['public_dir'])) {
                $config['public_dir'] = public_path();
            }

            return new Assets($config);
        });
    }
}
