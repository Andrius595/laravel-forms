<?php

namespace Andrius595\LaravelForms\Components;

class Radio extends FormsComponent
{
    public string $name;
    public array $options;
    public ?string $label;
    public mixed $checkedValue;
    public bool $required;
    public string $wrapperClass;
    public string $labelClass;
    public string $rowClass;
    public string $tooltip;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        string $name,
        array  $options,
        string $label = null,
        mixed  $checkedValue = null,
        bool   $required = false,
        string $wrapperClass = '',
        string $labelClass = '',
        string $rowClass = '',
        string $tooltip = ''
    )
    {
        $this->name = $name;
        $this->options = $options;
        $this->label = $label;
        $this->checkedValue = $checkedValue;
        $this->required = $required;
        $this->wrapperClass = $wrapperClass;
        $this->labelClass = $labelClass;
        $this->rowClass = $rowClass;
        $this->tooltip = $tooltip;
    }
}
