<?php

namespace Andrius595\LaravelForms\Components;

class File extends FormsComponent
{
    public string $name;
    public string $wrapperClass;
    public string $labelClass;
    public ?string $label;
    public mixed $file;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        string  $name,
        mixed   $file = null,
        ?string $label = null,
        string  $wrapperClass = '',
        string  $labelClass = ''
    )
    {
        $this->name = $name;
        $this->wrapperClass = $wrapperClass;
        $this->labelClass = $labelClass;
        $this->label = $label;
        $this->file = $file;
    }

}
