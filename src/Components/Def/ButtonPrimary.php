<?php

namespace Andrius595\LaravelForms\Components\Def;

use Andrius595\LaravelForms\Components\Button;

class ButtonPrimary extends Button
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $class = '', string $type = 'submit')
    {
        $class = 'uk-margin uk-button-primary ' . $class;
        parent::__construct($class, $type);
    }
}
