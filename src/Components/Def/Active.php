<?php

namespace Andrius595\LaravelForms\Components\Def;

use Andrius595\LaravelForms\Components\FormsComponent;

class Active extends FormsComponent
{
    public mixed $checkedValue;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(mixed $checkedValue = null)
    {
        $this->checkedValue = $checkedValue;
    }
}
