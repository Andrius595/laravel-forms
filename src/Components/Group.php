<?php

namespace Andrius595\LaravelForms\Components;

class Group extends FormsComponent
{
    public string $name;
    public ?string $label;
    public bool $required;
    public string $for;
    public string $wrapperClass;
    public string $labelClass;
    public string $rowClass;
    public string $tooltip;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        string $name,
        string $label = null,
        bool   $required = false,
        string $for = '',
        string $wrapperClass = '',
        string $labelClass = '',
        string $rowClass = '',
        string $tooltip = ''
    )
    {
        $this->name = $name;
        $this->label = $label;
        $this->required = $required;
        $this->for = $for;
        $this->wrapperClass = $wrapperClass;
        $this->labelClass = $labelClass;
        $this->rowClass = $rowClass;
        $this->tooltip = $tooltip;
    }
}
