<?php

namespace Andrius595\LaravelForms\Components;

use Closure;
use Illuminate\Contracts\View\View;

class Label extends FormsComponent
{
    public string $tooltip;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        string $tooltip = ''
    )
    {
        $this->tooltip = $tooltip;
    }
}
