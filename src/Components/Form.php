<?php

namespace Andrius595\LaravelForms\Components;

class Form extends FormsComponent
{
    public string $method;
    public bool $spoofMethod = false;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $method = 'POST')
    {
        $method = strtoupper($method);
        $this->method = $method;
        $this->spoofMethod = in_array($method, ['PUT', 'PATCH', 'DELETE']);
    }
}
