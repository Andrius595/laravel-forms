<?php

namespace Andrius595\LaravelForms\Components;

class Input extends FormsComponent
{
    public string $name;
    public ?string $label;
    public mixed $value;
    public bool $required;
    public ?string $id;
    public ?string $for;
    public string $wrapperClass;
    public string $labelClass;
    public string $rowClass;
    public string $tooltip;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        string $name,
        string $label = null,
        mixed  $value = null,
        bool   $required = false,
        string $id = null,
        string $wrapperClass = '',
        string $labelClass = '',
        string $rowClass = '',
        string $tooltip = ''
    )
    {
        $this->name = $name;
        $this->label = $label;
        $this->value = $value;
        $this->required = $required;
        $this->id = $id;
        $this->for = $id;

        if (null === $id) {
            $this->id = $name;
            $this->for = $name;
        }

        $this->wrapperClass = $wrapperClass;
        $this->labelClass = $labelClass;
        $this->rowClass = $rowClass;
        $this->tooltip = $tooltip;
    }

}
