<?php

namespace Andrius595\LaravelForms\Components;

use Closure;
use Illuminate\Contracts\Support\Htmlable;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Str;
use Illuminate\View\Component;

abstract class FormsComponent extends Component
{

    public function render(): array|View|Htmlable|string|Closure
    {
        $config = config("laravel-forms.components.{$this->componentName}");

        $framework = config("laravel-forms.framework");

        return str_replace('{framework}', $framework, $config['view']);
    }


    /**
     * Converts a bracket-notation to a dotted-notation
     *
     * @param string $name
     * @return string
     */
    protected static function convertBracketsToDots($name): string
    {
        return str_replace(['[', ']'], ['.', ''], $name);
    }
}
