<?php

namespace Andrius595\LaravelForms\Components;

use Illuminate\Support\Collection;

class SelectJs extends FormsComponent
{
    public string $name;
    public ?string $label;
    public array|Collection $options;
    public mixed $selected;
    public bool $multiple;
    public bool $required;
    public ?string $id;
    public ?string $for;
    public string $wrapperClass;
    public string $labelClass;
    public string $rowClass;
    public string $emptyValueLabel;
    public mixed $emptyValue;
    public string $tooltip;
    public string $allowClear;
    public string $ajaxCache;
    public string $ajaxDataType;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        string           $name,
        string           $label = null,
        array|Collection $options = [],
        mixed            $selected = null,
        bool             $multiple = false,
        bool             $required = false,
        string           $id = null,
        string           $wrapperClass = '',
        string           $labelClass = '',
        string           $rowClass = '',
        string           $emptyValueLabel = '',
        mixed            $emptyValue = '',
        string           $tooltip = '',
        string           $allowClear = 'true',
        string           $ajaxCache = 'true',
        string           $ajaxDataType = 'json'
    )
    {
        $this->name = $name;
        $this->label = $label;
        $this->options = $options;
        $this->selected = $selected;
        $this->multiple = $multiple;
        $this->required = $required;

        $this->id = $id;
        $this->for = $id;

        if (null === $id) {
            $this->id = $name;
            $this->for = $name;
        }

        $this->wrapperClass = $wrapperClass;
        $this->labelClass = $labelClass;
        $this->rowClass = $rowClass;
        $this->emptyValueLabel = $emptyValueLabel;
        $this->emptyValue = $emptyValue;
        $this->tooltip = $tooltip;
        $this->allowClear = $allowClear;
        $this->ajaxCache = $ajaxCache;
        $this->ajaxDataType = $ajaxDataType;
    }
}
