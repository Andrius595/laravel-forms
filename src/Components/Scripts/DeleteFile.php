<?php

namespace Andrius595\LaravelForms\Components\Scripts;

use Andrius595\LaravelForms\Components\FormsComponent;

class DeleteFile extends FormsComponent
{
    public string $ajaxUrl;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $ajaxUrl)
    {
        $this->ajaxUrl = $ajaxUrl;
    }
}
