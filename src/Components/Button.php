<?php

namespace Andrius595\LaravelForms\Components;

class Button extends FormsComponent
{
    public string $type;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(string $type = 'submit')
    {
        $this->type = $type;
    }
}
