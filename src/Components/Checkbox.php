<?php

namespace Andrius595\LaravelForms\Components;

class Checkbox extends FormsComponent
{
    public string $name;
    public mixed $value;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        string $name,
        mixed  $value = null
    )
    {
        $this->name = $name;
        $this->value = $value;
    }
}
