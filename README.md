# Form components

## Form

Attributes

* (string) method*
* $attributes

### <u>Example</u>

```html

<x-form id="form" class="class" action="{{ route('route') }}" method="method">
    ...
</x-form>
```

## Label

Attributes

* (string) tooltip
* $attributes

### <u>Example</u>

```html

<x-label for="id"
         class="class"
         tooltip="{{ __('tooltip') }}">
    @lang('label')
</x-label>
```

## Input

Attributes

* (string) name*
* (string) label
* (mixed) value
* (bool) required
* (string|null) id
* (string) wrapper-class
* (string) label-class
* (string) row-class
* (string) tooltip
* $attributes

### <u>Example</u>

```html

<x-input name="name"
         label="{{ __('label') }}"
         value="value"
         required
         placeholder="{{ __('placeholder') }"
         id="id"
         wrapper-class="wrapper-class"
         label-class="label-class"
         row-class="row-class"
         tooltip="{{ __('tooltip') }}"/>
```

## Hidden input

Attributes

* (string) name*
* (mixed) value*

### <u>Example</u>

```html

<x-input-hidden name="name" value="value"/>
```

## Checkbox

Attributes

* (string) name*
* (mixed) value
* $attributes

### <u>Example</u>

```html

<x-checkbox name="checkboxName" checked>
    @lang('label')
</x-checkbox>
```

## File / Delete File

File Attributes

* (string) name*
* (mixed) file
* (string|null) label
* (string) wrapper-class
* (string) label-class

Delete File Attributes

* (string) ajax-url*

### <u>Example</u>

```html

<x-file name="radioName"
        label="{{ __('label.translation') }}"
        :file="$file">
</x-file>

@push('scripts')
<x-delete-file ajax-url="{{ route('delete_file_route') }}">
</x-delete-file>
@endpush
```

## Radio

Attributes

* (string) name*
* (array) options*
* (string|null) label
* (mixed) checkedValue
* (bool) required
* (string) wrapper-class
* (string) label-class
* (string) row-class
* (string) tooltip
* $attributes

### <u>Example</u>

```html

<x-radio name="name"
         :options="['key' => 'value']"
         label="{{ __('label') }}"
         checkedValue="checkedValue"
         required
         wrapper-class="wrapper-class"
         label-class="label-class"
         row-class="row-class"
         tooltip="{{ __('tooltip') }}"/>
```

## Select

Attributes

* (string) name*
* (string|null) label
* (array|Illuminate\Support\Collection) options
* (mixed) selected
* (bool) multiple
* (bool) required
* (string|null) id
* (string) wrapper-class
* (string) label-class
* (string) row-class
* (mixed) empty-value
* (string) empty-value-label
* (string) tooltip
* $attributes

### <u>Example</u>

```html

<x-select name="name"
          label="{{ __('label') }}"
          :options="['key' => 'value']"
          selected="selected"
          id="id"
          wrapper-class="wrapper-class"
          label-class="label-class"
          row-class="row-class"
          empty-value=""
          empty-value-label="{{ __('empty.value_label') }}"
          multiple
          required
          tooltip="{{ __('tooltip') }}"/>
```

## Select JS

Attributes

* (string) name*
* (string|null) label
* (array|Illuminate\Support\Collection) options
* (mixed) selected
* (bool) multiple
* (bool) required
* (string|null) id
* (string) wrapper-class
* (string) label-class
* (string) row-class
* (mixed) empty-value
* (string) empty-value-label
* (string) tooltip
* (string) allow-clear
* (string) ajax-cache
* (string) ajax-data-type
* $attributes

### <u>Example</u>

```html

<x-select-js name="name"
             label="{{ __('label') }}"
             :options="['key' => 'value']"
             selected="selected"
             id="id"
             wrapper-class="wrapper-class"
             label-class="label-class"
             row-class="row-class"
             empty-value=""
             empty-value-label="{{ __('empty.value_label') }}"
             multiple
             required
             tooltip="{{ __('tooltip') }}"
             allow-clear="true"
             ajax-cache="true"
             ajax-data-type="json"/>
```

## Button

Attributes

* (string) type
* $attributes

### <u>Example</u>

```html

<x-button class="uk-button-large" type="button">
    @lang('label')
</x-button>
```

## Textarea

Attributes

* (string) name*
* (string) label
* (bool) required
* (string|null) placeholder
* (string|null) id
* (string) wrapper-class
* (string) label-class
* (string) row-class
* (string) tooltip
* (string) rows
* $attributes

### <u>Example</u>

```html

<x-textarea name="name"
            label="{{ __('label') }}"
            required
            placeholder="{{ __('placeholder') }"
            id="id"
            wrapper-class="wrapper-class"
            label-class="label-class"
            row-class="row-class"
            tooltip="{{ __('tooltip') }}"
            rows="5">
    Content
</x-textarea>
```

## Rich Textarea (Summernote)

Attributes

* (string) name*
* (string|null) label
* (bool) required
* (string|null) placeholder
* (string|null) id
* (string) wrapper-class
* (string) label-class
* (string) row-class
* (string) tooltip
* $attributes

### <u>Example</u>

```html

<x-rich-textarea name="name"
                 label="{{ __('label') }}"
                 required
                 placeholder="{{ __('placeholder') }"
                 id="id"
                 wrapper-class="wrapper-class"
                 label-class="label-class"
                 row-class="row-class"
                 tooltip="{{ __('tooltip') }}"
                 rows="5">
    Content
</x-rich-textarea>
```

## Defined fields

### <u>Active</u>

Attributes

* (mixed) checked-value

```html

<x-def-active checked-value="{{ old('active', '1') }}"/>
```
