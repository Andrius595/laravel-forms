<?php

namespace Andrius595\LaravelForms\Tests;

use Andrius595\LaravelForms\Support\Facade;
use Andrius595\LaravelForms\Support\ServiceProvider;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;
use Orchestra\Testbench\BrowserKit\TestCase as BaseTestCase;


class TestCase extends BaseTestCase
{
    protected $baseUrl = 'http://localhost';

    public function setUp(): void
    {
        parent::setUp();

        View::addLocation(__DIR__ . '/Feature/views');
    }

    /** Registers routes */
    protected function defineEnvironment($app)
    {

    }

    protected function getPackageAliases($app)
    {
        return [
            'FormsAssets' => Facade::class,
        ];
    }

    protected function getPackageProviders($app)
    {
        return [
            ServiceProvider::class,
        ];
    }

    protected function registerTestRoute($uri, callable $post = null): self
    {
        Route::middleware('web')->group(function () use ($uri, $post) {
            Route::view($uri, $uri);

            if ($post) {
                Route::post($uri, $post);
            }
        });

        return $this;
    }
}
