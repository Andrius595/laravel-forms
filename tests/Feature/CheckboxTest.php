<?php

namespace Andrius595\LaravelForms\Tests\Feature;

use Andrius595\LaravelForms\Tests\TestCase;
use Illuminate\Http\Request;

class CheckboxTest extends TestCase
{
    /** @test */
    public function renders_checkbox_element()
    {
        $this->registerTestRoute('checkbox-page');

        $this->visit('checkbox-page')
            ->seeElement('input[name="first"][value="a"]:not(:checked)')
            ->seeElement('input[name="checked"][value="b"]:checked');
    }

    ///** @test */
    public function loads_old_data()
    {
        $this->registerTestRoute('checkbox-page', function(Request $request) {
            $request->validate([
                'first' => 'required',
                'checked' => 'required',
            ]);
        });
        $this->visit('checkbox-page')
            ->seeElement('input[name="first"][value="a"]:not(:checked)')
            ->seeElement('input[name="checked"][value="b"]:checked')
            ->check('first')

            ->seeElement('input[name="first"][value="a"]:checked')
            ->seeElement('input[name="checked"][value="b"]:checked');
    }


}
