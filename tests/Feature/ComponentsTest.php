<?php

namespace Andrius595\LaravelForms\Tests\Feature;

use Andrius595\LaravelForms\Tests\TestCase;

class ComponentsTest extends TestCase
{
    /** @test */
    public function renders_components()
    {
        $this->registerTestRoute('components');

        $this->visit('components')
            ->seeElement('form')
            ->seeElement('button[type="submit"]')
            ->seeElement('input[type="checkbox"]')
            ->seeInElement('div.uk-text-danger', "Error")
            ->seeElement('input[type="file"]')
            ->seeElement('input[name="input"]')
            ->seeElement('input[type="hidden"][name="hidden"][value="h-value"]')
            ->seeElement('label[for="input"]')
            ->seeElement('input[type="radio"][name="radio"]')
            ->seeElement('textarea[name="rich-textarea"]') // Does not check if summernote is initialized
            ->seeElement('select[name="select"]')
            ->seeElement('select[name="select-js"]')   // Does not check if select2 is initialized
            ->seeElement('textarea[name="textarea"]');
    }

    /** @test */
    public function form_component_renders_correctly()
    {
        $this->registerTestRoute('get-form-page');
        $this->registerTestRoute('post-form-page');
        $this->registerTestRoute('put-form-page');
        $this->registerTestRoute('delete-form-page');

        $this->visit('get-form-page')
            ->seeElement('form#get-form')
            ->seeInElement('form#get-form', 'Content')
            ->dontSeeElement('input[type="hidden"][name="_token"]')
            ->dontSeeElement('input[type="hidden"][name="_method"]');

        $this->visit('post-form-page')
            ->seeElement('form#post-form')
            ->seeInElement('form#post-form', 'Content')
            ->seeElement('input[type="hidden"][name="_token"]')
            ->dontSeeElement('input[type="hidden"][name="_method"]');

        $this->visit('put-form-page')
            ->seeElement('form#put-form')
            ->seeInElement('form#put-form', 'Content')
            ->seeElement('input[type="hidden"][name="_token"]')
            ->seeElement('input[type="hidden"][name="_method"][value="PUT"]');

        $this->visit('delete-form-page')
            ->seeElement('form#delete-form')
            ->seeInElement('form#delete-form', 'Content')
            ->seeElement('input[type="hidden"][name="_token"]')
            ->seeElement('input[type="hidden"][name="_method"][value="DELETE"]');

    }

    /** @test */
    public function button_component_renders_correctly()
    {
        $this->registerTestRoute('button-page');

        $this->visit('button-page')
            ->seeElement('button.custom-class[type="submit"]')
            ->seeInElement('button[type="submit"]', 'Submit')
            ->seeElement('button[type="button"]')
            ->seeInElement('button[type="button"]', 'Button');
    }

    /** @test */
    public function renders_checkbox_component_correctly()
    {
        $this->registerTestRoute('checkbox-page');

        $this->visit('checkbox-page')
            ->seeElement('input[name="first"][value="a"]:not(:checked)')
            ->dontSeeElement('input[name="first"][value="a"]:checked')
            ->seeElement('input[name="checked"][value="b"]:checked')
            ->dontSeeElement('input[name="checked"][value="b"]:not(:checked)');
    }

    /** @test */
    public function renders_file_component_correctly()
    {
        $this->registerTestRoute('file-page');

        $this->visit('file-page')
            ->seeElement('input[type="file"][name="with-file"]')
            ->seeInElement('label[for="with-file"]', 'File1')
            ->seeElement('a[data-image="with-file"]')
            ->seeElement('input[type="file"][name="without-file"]')
            ->seeInElement('label[for="without-file"]', 'File2')
            ->dontSeeElement('a[data-image="without-file"]');
    }

    /** @test */
    public function renders_input_component_correctly()
    {
        $this->registerTestRoute('input-page');

        $this->visit('input-page')
            ->seeElement('input#first[name="first"]')
            ->dontSeeElement('input#first[name="first"][required]')
            ->dontSeeElement('label[for="first"]')
            ->seeElement('input#second-input[name="second"][placeholder="Second..."][value="Second Value"][required]')
            ->seeElement('label[for="second-input"]')
            ->seeInElement('label[for="second-input"]', 'Second Label');
    }

    /** @test */
    public function radio_component_renders_correctly()
    {
        $this->registerTestRoute('radio-page');

        $this->visit('radio-page')
            ->seeInElement('label.uk-form-label', 'radio group')
            ->seeInElement('label', 'first')
            ->seeInElement('label', 'second')
            ->dontSeeIsSelected('radio', 'a')
            ->seeIsSelected('radio', 'b');
    }

    /** @test */
    public function rich_textarea_component_renders_correctly()
    {
        $this->registerTestRoute('rich-textarea-page');

        $this->visit('rich-textarea-page')
            ->seeElement('textarea#first[name="first"]')
            ->dontSeeElement('label[for="first"]')
            ->seeElement('textarea#second-rta[name="second"][required]')
            ->seeInElement('label[for="second-rta"]', 'Second RTA')
            ->see('code.jquery.com')
            ->seeElement('script[src="js/vendor/laravel-forms/scripts.js"]')
            ->see('summernote-lite.min.css')
            ->see('summernote-lite.min.js');
    }

    /** @test */
    public function select_component_renders_correctly()
    {
        $this->registerTestRoute('select-page');

        $this->visit('select-page')
            ->seeElement('select#empty[name="empty"]')
            ->dontSeeElement('label[for="empty"]')
            ->seeElement('select#select-component[name="select"]')
            ->seeElement('option[value="a"]')
            ->seeInElement('option[value="a"]', 'first')
            ->seeElement('option[value="b"]')
            ->seeInElement('option[value="b"]', 'second')
            ->seeIsSelected('select', 'b')
            ->seeInElement('label[for="select-component"]', 'select group');
    }

    /** @test */
    public function select_js_component_renders_correctly()
    {
        $this->registerTestRoute('select-js-page');

        $this->visit('select-js-page')
            ->seeElement('select#empty[name="empty"]')
            ->dontSeeElement('label[for="empty"]')
            ->seeElement('select#select-component[name="select"][data-placeholder="empty-value"]')
            ->seeElement('option[value="a"]')
            ->seeInElement('option[value="a"]', 'first')
            ->seeElement('option[value="b"]')
            ->seeInElement('option[value="b"]', 'second')
            ->seeIsSelected('select', 'b')
            ->seeInElement('label[for="select-component"]', 'select group')
            ->see('code.jquery.com')
            ->seeElement('script[src="js/vendor/laravel-forms/scripts.js"]')
            ->see('select2.min.js');
    }

    /** @test */
    public function textarea_component_renders_correctly()
    {
        $this->registerTestRoute('textarea-page');

        $this->visit('textarea-page')
            ->seeElement('textarea#first[name="first"]')
            ->dontSeeElement('label[for="first"]')
            ->seeElement('textarea#second-rta[name="second"][required]')
            ->seeInElement('label[for="second-rta"]', 'Second RTA');
    }

    /** @test */
    public function delete_file_component_renders_correctly()
    {
        $this->registerTestRoute('delete-file-page');

        $this->visit('delete-file-page')
            ->seeElement('script')
            ->see('url: \'file-url\'');
    }
}
