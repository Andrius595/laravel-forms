<?php

namespace Andrius595\LaravelForms\Tests\Feature;

use Andrius595\LaravelForms\Tests\TestCase;
use Illuminate\Http\Request;

class RadioTest extends TestCase
{
    /** @test */
    public function radio_component_renders_correctly()
    {
        $this->registerTestRoute('radio-page', function (Request $request) {
            $request->validate([
                'radio' => 'required|in:a',
            ]);
        });
        $this->visit('/radio-page')
            ->seeInElement('label.uk-form-label', 'radio group')
            ->seeInElement('label', 'first')
            ->seeInElement('label', 'second')
            ->dontSeeIsSelected('radio', 'a')
            ->seeIsSelected('radio', 'b');
    }
}
