@extends('app')

@section('content')
    <x-rich-textarea name="first"></x-rich-textarea>
    <x-rich-textarea name="second" id="second-rta" label="Second RTA" required></x-rich-textarea>
@endsection
