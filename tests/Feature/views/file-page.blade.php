@extends('app')

@section('content')

    <x-file name="with-file" label="File1" file="file.png"></x-file>
    <x-file name="without-file" label="File2"></x-file>

@endsection
