@extends('app')

@section('content')

    <x-input name="first"></x-input>
    <x-input name="second"
             label="Second Label"
             value="Second Value"
             required
             placeholder="Second..."
             id="second-input">
    </x-input>


@endsection
