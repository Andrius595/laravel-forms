@extends('app')

@section('content')

    <x-select-js name="empty"></x-select-js>
    <x-select-js name="select"
                 id="select-component"
                 :options="['a' => 'first', 'b' => 'second']"
                 selected="b"
                 label="select group"
                 empty-value="-"
                 empty-value-label="empty-value"></x-select-js>

@endsection
