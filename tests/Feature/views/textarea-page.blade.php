@extends('app')

@section('content')
    <x-textarea name="first"></x-textarea>
    <x-textarea name="second" id="second-rta" label="Second RTA" required></x-textarea>
@endsection
