@extends('app')

@section('content')
    <x-form> //Form
        <x-button>Submit</x-button> //Button, type submit
        <x-checkbox name="checkbox"></x-checkbox> //input, type checkbox
        <x-error>Error</x-error> //div, class uk-text-danger, slot
        <x-file name="file"></x-file> //input, type file
        <x-input name="input"></x-input> //group. input, name input
        <x-input-hidden name="hidden" value="h-value"></x-input-hidden> //input, type hidden
        <x-label for="input"></x-label> //label, for input
        <x-radio name="radio" :options="[1,2]"></x-radio> // radio, value 1. radio, value 2
        <x-rich-textarea name="rich-textarea"></x-rich-textarea> //textarea, name rich-textarea. div, class note-editor node-frame
        <x-select name="select"></x-select> //select, name select
        <x-select-js name="select-js"></x-select-js> //select, name select-js
        <x-textarea name="textarea"></x-textarea> //textarea, name textarea
    </x-form>
@endsection

