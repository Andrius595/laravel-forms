@extends('app')

@section('content')
    <x-form>
        <x-radio name="radio" :options="['a' => 'first', 'b' => 'second']" checked-value="b" label="radio group"></x-radio>

        <x-button>Submit</x-button>
    </x-form>
@endsection
