@extends('app')

@section('content')

    <x-select name="empty"></x-select>
    <x-select name="select"
              id="select-component"
              :options="['a' => 'first', 'b' => 'second']"
              selected="b"
              label="select group"></x-select>

@endsection
