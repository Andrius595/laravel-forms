<?php

use Andrius595\LaravelForms\Components;

return [
    /** Load assets before rendering page */
    'autoload'    => ['uikit'],

    /** Assets collections */
    'collections' => [
        'uikit'  => [
            'https://cdn.jsdelivr.net/npm/uikit@3.8.0/dist/css/uikit.min.css',
            'https://cdn.jsdelivr.net/npm/uikit@3.8.0/dist/js/uikit.min.js',
            'https://cdn.jsdelivr.net/npm/uikit@3.8.0/dist/js/uikit-icons.min.js',
        ],

        /** Comment out jquery if you're already using it in your application */
        'jquery' => 'https://code.jquery.com/jquery-3.6.0.min.js',

        'scripts' => [
            'jquery',
            'vendor/laravel-forms/scripts.js',
        ],

        'summernote' => [
            'jquery',
            'scripts',
            'https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css',
            'https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js',
        ],

        'select2' => [
            'jquery',
            'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css',
            'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js',
        ],

    ],

    /** Options: 'ui-kit' */
    'framework'   => 'ui-kit',

    /** Components collections */
    'components'  => [
        'form'          => [
            'view'  => 'laravel-forms::{framework}.form',
            'class' => Components\Form::class,
        ],
        'label'         => [
            'view'  => 'laravel-forms::{framework}.label',
            'class' => Components\Label::class,
        ],
        'input'         => [
            'view'  => 'laravel-forms::{framework}.input',
            'class' => Components\Input::class,
        ],
        'input-hidden'  => [
            'view'  => 'laravel-forms::{framework}.input-hidden',
            'class' => Components\InputHidden::class,
        ],
        'checkbox'      => [
            'view'  => 'laravel-forms::{framework}.checkbox',
            'class' => Components\Checkbox::class,
        ],
        'file'          => [
            'view'  => 'laravel-forms::{framework}.file',
            'class' => Components\File::class,
        ],
        'radio'         => [
            'view'  => 'laravel-forms::{framework}.radio',
            'class' => Components\Radio::class,
        ],
        'select'        => [
            'view'  => 'laravel-forms::{framework}.select',
            'class' => Components\Select::class,
        ],
        'select-js'     => [
            'view'  => 'laravel-forms::{framework}.select-js',
            'class' => Components\SelectJs::class,
        ],
        'button'        => [
            'view'  => 'laravel-forms::{framework}.button',
            'class' => Components\Button::class,
        ],
        'textarea'      => [
            'view'  => 'laravel-forms::{framework}.textarea',
            'class' => Components\Textarea::class,
        ],
        'rich-textarea' => [
            'view'  => 'laravel-forms::{framework}.rich-textarea',
            'class' => Components\RichTextarea::class,
        ],
        'def-active'    => [
            'view'  => 'laravel-forms::{framework}.def.active',
            'class' => Components\Def\Active::class,
        ],
        'error'         => [
            'view'  => 'laravel-forms::{framework}.error',
            'class' => Components\Error::class,
        ],
        'group'         => [
            'view'  => 'laravel-forms::{framework}.group',
            'class' => Components\Group::class,
        ],
        'delete-file'         => [
            'view'  => 'laravel-forms::{framework}.scripts.delete-file',
            'class' => Components\Scripts\DeleteFile::class,
        ]
    ]
];
